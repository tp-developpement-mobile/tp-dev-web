import React from 'react';
import { Linking, StyleSheet, Text, View } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import Header from '../components/Header';



class MapInfoScreen extends React.Component {
    constructor(props){
        super(props)
    }

    showDrawer() {
        this.props.navigation.toggleDrawer();
    }

    render(){
        const centre = this.props.route.params
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Header showDrawer={this.showDrawer.bind(this)}></Header>
                </View>
                <View style={styles.content}>
                    <MapView style={{flex: 1}}
                        initialRegion={{
                            latitude: centre.properties.c_lat_coor1,
                            longitude: centre.properties.c_long_coor1,
                            latitudeDelta: 0.005,
                            longitudeDelta: 0.005
                        }}
                    >
                        <Marker coordinate={{ latitude: centre.properties.c_lat_coor1, longitude: centre.properties.c_long_coor1 }}
                            pinColor = "#a29bfe" />
                    </MapView>
                    <Text style={styles.bold}>
                        {centre.properties.c_nom}
                        {"\n"}
                    </Text>
                    <Text style={styles.underline} >
                        Horaires :
                    </Text>
                    <Text style={styles.text}>
                        {"\n"}
                        Lundi : {centre.properties.c_rdv_lundi}
                        {"\n"}
                        Mardi : {centre.properties.c_rdv_mardi}
                        {"\n"}
                        Mercredi : {centre.properties.c_rdv_mercredi}
                        {"\n"}
                        Jeudi : {centre.properties.c_rdv_jeudi}
                        {"\n"}
                        Vendredi : {centre.properties.c_rdv_vendredi}
                        {"\n"}
                        Samedi : {centre.properties.c_rdv_samedi}
                        {"\n"}
                        Dimanche : {centre.properties.c_rdv_dimanche}
                        {"\n"}
                    </Text>
                    <Text style={styles.underline}>
                        Contacts :
                    </Text>
                    <Text style={styles.text}>
                        Téléphone : {centre.properties.c_rdv_tel}
                        {"\n"}
                    </Text>
                    <Text style = {{color: 'blue', textDecorationLine: 'underline'}} onPress = { () => {Linking.openURL(centre.properties.c_rdv_site_web)}}>
                        Site web : {centre.properties.c_rdv_site_web}
                        {"\n"}
                    </Text>
                    <Text style={styles.underline}>
                        Adresse:
                    </Text>
                    <Text style={styles.text}>
                        {centre.properties.c_adr_num}  {centre.properties.c_adr_voie}
                    </Text>
                </View>
            </View>     
        )
    }
}

const styles = StyleSheet.create({

    bold: {
        fontWeight: 'bold',
        fontSize: 20,
        marginTop: 5,
        color: 'white',
        textAlign: 'center'
    },
    italic: {fontStyle: 'italic'},
    underline: {
        textDecorationLine: 'underline',
        fontSize: 15,
        color: 'white',
        marginLeft: 5
    },
    text:{
        color:'white',
        marginLeft: 15
    },
    container: {
        flex: 1,
        backgroundColor: '#a29bfe',
        justifyContent: 'flex-end',
    },
    header:{
        flex: 1,
    },
    content:{
        flex: 8,
    },
    mapPartial: {
        marginLeft: 60,
        marginBottom: 200,
        width: 300,
        height: 300,
    },
});

export default MapInfoScreen
