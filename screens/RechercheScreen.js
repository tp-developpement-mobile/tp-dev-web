import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import Header from '../components/Header';
import ListeCentres from '../components/ListeCentres';

class RechercheScreen extends React.Component {
    constructor(props){
        super(props)
    }

    showDrawer() {
        this.props.navigation.toggleDrawer();
    }

    goToMapInfoScreen(params){
        this.props.navigation.navigate('Détails du centre', params)
    }

    render(){
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <Header showDrawer={this.showDrawer.bind(this)}></Header>
                </View>
                <View style={styles.content} >
                    <ListeCentres Ville={this.props.route.params.Ville} goToMapInfoScreen = {this.goToMapInfoScreen.bind(this)}/>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dcdcdc',
        justifyContent: 'flex-end',
    },
    header:{
        flex: 1
    },
    content:{
        flex: 8
    }
});

export default RechercheScreen
