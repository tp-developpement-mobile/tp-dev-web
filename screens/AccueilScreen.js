import React from 'react';
import {Button, TextInput, Text, StyleSheet, View } from 'react-native';
import Header from '../components/Header';
import Logo_vaccin from '../components/Logo_vaccin';

class AccueilScreen extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            text : ""
        }
    }

    showDrawer() {
        this.props.navigation.toggleDrawer();
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <Header showDrawer={this.showDrawer.bind(this)}></Header>
                </View>
                <View style={styles.content}>
                    <TextInput
                        style={styles.search}
                        placeholder="  Entrer votre ville de recherche !"
                        onChangeText={input => this.setState({text: input})}
                        defaultValue={this.state.text}
                    /> 
                    <Button 
                        style={styles.Button} 
                        type="clear"  
                        color="#bdc3c7" title="Search" 
                        onPress = { () => {
                                if (this.state.text == "") {
                                    alert("Veuillez entrer une ville");
                                }else{
                                    this.props.navigation.navigate('Liste des centres', { Ville: this.state.text })
                                }
                            }
                        } 
                    />
                    
                </View>

                <Logo_vaccin/> 

                <View>
                     
                    <Text style={styles.text2}>Application développée par</Text>
                    <Text style={styles.text}>LAFAVERGES Quentin MAGNIN Achille TANTOT Lilian</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'flex-end',
    },
    header: {
        flex: 2
    },
    content: {
        flex: 8
    },
    search: {
        borderTopWidth: 1,
        borderColor: '#a29bfe',
        borderWidth: 1,
        borderRadius: 5,
        height: 50
    },
    text: {
        borderBottomWidth: 10,
        textAlign: "center",
        color: "#bdc3c7",
        fontSize: 11,
    },
    text2: {
        textAlign: "center",
        color: "#bdc3c7",
        fontSize: 11,
    },
});

export default AccueilScreen
