import React from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import AccueilScreen from '../screens/AccueilScreen';
import RechercheScreen from '../screens/RechercheScreen';
import MapInfoScreen from '../screens/MapInfoScreen';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

class Navigator extends React.Component {
    constructor(props){
        super(props)
    }

    render(){
        return (
            <Drawer.Navigator>
                <Drawer.Screen name="Accueil" component={AccueilScreen}/>
                <Drawer.Screen name="Liste des centres" component={RechercheScreen}/>
                <Drawer.Screen name="Détails du centre" component={MapInfoScreen}/>
            </Drawer.Navigator>  
        )
    }

    Stack(){
        return(
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Accueil">
                    <Stack.Screen name="Accueil" component={AccueilScreen} />
                    <Stack.Screen name="Liste des centres" component={RechercheScreen} />
                    <Stack.Screen name="Détails du centre" component={MapInfoScreen} />
                </Stack.Navigator> 
            </NavigationContainer>
        )
    }
}

export default Navigator
