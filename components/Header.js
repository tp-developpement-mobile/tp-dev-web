import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import Logo from './Logo';

class Header extends React.Component {
    constructor(props){
        super(props)
    }

    showDrawer() {
        navigation.openDrawer()
    }

    render(){
        return (
            <View style={styles.containerheader}>
                <Button style={styles.button} color="#bdc3c7" title="=" onPress={this.props.showDrawer}/>
                <Text style={styles.text} >Vaccin finder</Text>
                <Logo/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
      marginLeft: 30,
      color: "white",
      fontSize: 20, 
      textAlign: "center",
      flex: 5
    },
    button: {
        flex: 1, 
        alignItems: "center",
    }, 
    containerheader: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#a29bfe',
        alignItems: "center", 
        justifyContent: "center"
    }
});

export default Header
