import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import logo_vaccin from '../assets/logo_vaccin.png';

class Logo_vaccin extends React.Component {
    constructor(props){
        super(props)
    }

    render(){
        return (
            <View style={styles.container}>
                <Image 
                    style={styles.image} 
                    source={logo_vaccin}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
        
    image: {        
        width: 400,
        height: 300,
        resizeMode: 'contain'     
    },
});

export default Logo_vaccin
