import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import logo from '../assets/logo.png';

class Logo extends React.Component {
    constructor(props){
        super(props)
    }

    render(){
        return (
            <View style={styles.container}>
                <Image 
                    style={styles.logo} 
                    source={logo}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'flex-end',
        marginTop: 20
    },
    logo: { 
        width: 55,
        height: 45,
    },
});

export default Logo
