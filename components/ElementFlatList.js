import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, navigation, onPress } from 'react-native';
import 'react-native-gesture-handler';


class ElementFlatList extends React.Component {
    constructor(props){
        super(props)
    }
    
    
    showDrawer() {
        this.props.navigation.toggleDrawer();
    } 
    
    render(){
        
        return (
             
            <View style={styles.card}>
                <View style={{alignItems:"center", flex:1}}>  
                    
                    <TouchableOpacity 
                        style={styles.button} 
                        onPress={ () => this.props.goToMapInfoScreen(this.props.centre) }
                    >  
                    
                        <Text style={styles.textNom}>{this.props.centre.properties.c_nom}</Text>
                        <Text style={styles.text}>
                            {"Adresse : " + this.props.centre.properties.c_adr_num + " " 
                                            + this.props.centre.properties.c_adr_voie + " " 
                                            + this.props.centre.properties.c_com_nom}
                        </Text>
                    </TouchableOpacity> 
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    card: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        textAlign: "left",
        margin: 10,
        padding: 12,
    },
    text: {
        color: "black",
        fontSize: 16,
    },
    textNom: {
        textAlign: "left",
        fontWeight: "bold",
        color: "black",
        fontSize: 16,
    },
});

export default ElementFlatList
