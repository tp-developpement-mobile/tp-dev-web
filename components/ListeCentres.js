import React from 'react';
import { StyleSheet, FlatList, View } from 'react-native';
import ListeCentreVaccination from '../services/ListeCentreVaccination';
import ElementFlatList from './ElementFlatList';

class ListeCentres extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            centresVaccination : [],
        }

        this.getListeCentreVaccinationTri();
    }

    getListeCentreVaccinationTri() {
        let centre = new ListeCentreVaccination(); 
        centre.getListeCentreVaccination().then( (centreVaccination) => {
            let currentData = this.state.centresVaccination;
            currentData = centreVaccination.filter( (element) => { 
                return element.properties.c_com_nom == this.props.Ville;
            })
            this.setState({
                centresVaccination : currentData
            })
        })
    }
    proxyStatelessComponent( props ){
        return (   
            <View> 
                <ElementFlatList centre={props.item} goToMapInfoScreen = {this.props.goToMapInfoScreen}/> 
            </View>
            )
    }

    render(){
        return (
            <View>               
                <FlatList
                    data = {this.state.centresVaccination}
                    renderItem={this.proxyStatelessComponent.bind(this)}
                    keyExtractor={(item, id) => id.toString()}
                />
            </View>
        )
    }
}

export default ListeCentres
