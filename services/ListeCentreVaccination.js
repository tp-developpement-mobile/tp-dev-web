import fetch from 'node-fetch';

const LISTE_CENTRE_VACCINATION_API = "https://www.data.gouv.fr/fr/datasets/r/d0566522-604d-4af6-be44-a26eefa01756";

class ListeCentreVaccination {
    getListeCentreVaccination(){
        return new Promise( (resolve,reject) => {
            fetch(LISTE_CENTRE_VACCINATION_API).then( httpResponse => {
                httpResponse.json().then( apiResponse => {
                    console.log(apiResponse);
                    resolve(apiResponse.features);
                })
            })
        })
    }
}

export default ListeCentreVaccination